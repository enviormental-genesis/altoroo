import React, {useState} from "react";
import {Container, FormControl, FormGroup, Input, Button, InputLabel} from "@material-ui/core";
import axios from 'axios'


const Signup = () => {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [pass, setPass] = useState('')
    const [pass2, setPass2] = useState('')

    const [nameError, setNameError] = useState(false)
    const [emailError, setEmailError] = useState(false)
    const [passError, setPassError] = useState(false)

    const dispatch = async () => {
        setEmailError(false)
        setNameError(false)
        setPassError(false)

        if (pass !== pass2){
            setPassError(true)
            return
        }
        const payload = {name: name, email: email, pass: pass}
        await axios.post('/api/auth', payload)
            .then(() => {
                window.location.href = '/';
            }).catch(err => {
                switch (err.response.data.type){
                    case '["email"]': setEmailError(true); break;
                    case '["name"]': setNameError(true); break;
                    case '["name","email"]': setNameError(true); setEmailError(true); break;
                    default: setPassError(true); setEmailError(true); setNameError(true);
                }
            })
    }


    return(
        <Container maxWidth="xs">
            <FormGroup>
                <FormControl><InputLabel htmlFor="name">Name: </InputLabel><Input error={nameError} required id="name" placeholder='James Smith' onChange={e => setName(e.target.value)}/></FormControl>
                <FormControl><InputLabel htmlFor="email">Email: </InputLabel><Input error={emailError} required id="email" placeholder='example@domain.com' onChange={e => setEmail(e.target.value)}/></FormControl>
                <FormControl><InputLabel htmlFor="pass">Password: </InputLabel><Input error={passError} required id="pass" placeholder='********' onChange={e => setPass(e.target.value)}/></FormControl>
                <FormControl><InputLabel htmlFor="pass2">Password again: </InputLabel><Input error={passError} required id="pass2" placeholder='********' onChange={e => setPass2(e.target.value)}/></FormControl>
                    <br/><br/>
                <Button variant="contained" color="primary" onClick={dispatch}>Sign up</Button>
                    <br/><br/><br/><br/>
                <Button variant="contained" color="primary" onClick={()=> {window.location.href = '/login'}}>Or would you like to log in</Button>

            </FormGroup>
        </Container>
    );

}

export default Signup;