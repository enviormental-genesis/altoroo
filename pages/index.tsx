import React, {useEffect, useState} from 'react'
import { Text, TextContainer } from 'react-md'
import axios from "axios";
import { useCookies } from 'react-cookie';
import {Button} from "@material-ui/core";

const Home = () => {
    const [,, removeCookie] = useCookies(['altorootoken']);
    const [name, setName] = useState<null|string>(null)

    const logout = () => {
        removeCookie("altorootoken");
        window.location.href = '/login'
    }

    useEffect(() => {
        axios.get('/api/auth').then(res=>{
            setName(res.data.name)
        }).catch(() => {
            logout()
        })
    }, [])



  return (
    <TextContainer>
      <Text type="headline-4">Hello, {name}</Text>
        <br/><br/><br/><br/>
        <Button variant="contained" color="primary" onClick={logout}>log out</Button>
    </TextContainer>
  )
}

export default Home