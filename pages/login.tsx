import React, {useState} from "react";
import {Container, FormControl, FormGroup, Input, Button, InputLabel} from "@material-ui/core";
import axios from 'axios'

const Login = () => {

    const [email, setEmail] = useState('')
    const [pass, setPass] = useState('')

    const [authError, setAuthError] = useState(false)

    const dispatch = async () => {
        setAuthError(false)

        await axios.get('/api/auth', { auth : { username: email, password: pass } })
            .then(() => {
                window.location.href = '/';
        }).catch(error => {
                setAuthError(true)
                console.log(error.response.data)
            })

    }

    return(
        <Container maxWidth="xs">
            <FormGroup>
                <FormControl><InputLabel htmlFor="email">Email: </InputLabel><Input error={authError} required id="email" placeholder='example@domain.com' onChange={e => setEmail(e.target.value)}/></FormControl>
                <FormControl><InputLabel htmlFor="pass">Password: </InputLabel><Input error={authError} required id="pass" placeholder='********' onChange={e => setPass(e.target.value)}/></FormControl>
                <br/><br/>
                <Button variant="contained" color="primary" onClick={dispatch}>Log in</Button>
                <br/><br/><br/><br/>
                <Button variant="contained" color="primary" onClick={()=> {window.location.href = '/signup'}}>Or would you like to sign up</Button>
            </FormGroup>
        </Container>
    );



}

export default Login