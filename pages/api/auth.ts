import { User } from './_models'
import bcrypt from 'bcrypt';
import assert from "assert";
import Cookies from 'cookies'

const handler = async (req: any, res: any) => {

    if (req.method === 'GET') {
        const cookies = new Cookies(req, res)
        const rawToken = cookies.get('altorootoken')  || req.headers.authorization
        if (!rawToken){
            return res.status(401).json({type: 'noAuth'});
        }
        const token = rawToken.split(" ");
        const [email, pass] = Buffer.from(token[1], 'base64').toString().split(":")


        await User.findOne({email: email}).then((user: any) => {
            assert(!!user && bcrypt.compareSync(pass, user.pass),'noAuth')
            cookies.set('altorootoken', rawToken)
            return res.status(200).json(user);
        }).catch(err =>{
            cookies.set('altorootoken')
            return res.status(401).json({type: err.message});
        });
    }

    if (req.method === 'POST') {
        const body = bcrypt.genSalt(10).then(salt => {
            return bcrypt.hash(req.body.pass, salt)
        }).then(hash => {
            const body = req.body;
            body.pass = hash;
            body.salt = hash;
            return body
        })


        const user = new User(await body)
        user.save().then(() => {
            const rawToken = 'Basic ' + new Buffer(user.email + ':' + user.pass).toString('base64');
            const cookies = new Cookies(req, res)
            cookies.set('altorootoken', rawToken)
            return res.status(201).json(user)
        }).catch((err: { _message: any; errors: {}; }) => {
            switch (err._message) {
                case 'users validation failed':
                    return res.status(409).json({type: JSON.stringify(Object.keys(err.errors))});
                default:
                    return res.status(500);
            }
        })
    }
}

export default handler