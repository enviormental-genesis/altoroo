import { model, models, Schema } from "mongoose";
const mongoose = require('mongoose');
require('mongoose-type-email');
import uniqueValidator from 'mongoose-unique-validator';

mongoose.Promise = global.Promise;

mongoose.connect(process.env.CONNSTRING, {useNewUrlParser: true, useUnifiedTopology: true,
    authSource: 'admin', useCreateIndex: true},{})

const userSchema: Schema = new Schema({
        name: {
            type: String,
            required: true,
            unique: true
        },
        email: {
            type: mongoose.SchemaTypes.Email,
            required: true,
            unique: true
        },
        pass: {
            type: String,
            required: true
        },
        salt: {
            type: String,
            required: true
        }},
    {
        collection: "users",
    });
userSchema.plugin(uniqueValidator)
export const User = models.users || model("users", userSchema);
