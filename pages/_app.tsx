import React, { ReactElement } from 'react'
import Head from 'next/head'
import { AppProps } from 'next/app'
import '../styles/app.scss'
import '../styles/stylesheet.css'
import {Container} from "@material-ui/core";

export default function App({ Component, pageProps }: AppProps): ReactElement {
  return (
    <Container>
      <Head>
        <title>react-md with next.js</title>
      </Head>
        <br/><br/><br/>
      <Component {...pageProps} />
    </Container>
  )
}
